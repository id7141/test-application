const express = require('express')
const path = require('path');

const app = express();
const PORT = 3535;
const dirName = path.resolve();
app.use(express.static(path.resolve(dirName, 'public')));

app.get('/uk', (req, res) => {
	res.sendFile(path.resolve(dirName, 'public', 'uk.html'))
})

app.get('/en', (req, res) => {
	res.sendFile(path.resolve(dirName, 'public', 'en.html'))
})

app.listen(PORT, () => {
	console.log(`server has been started on ${PORT}...`)
});
